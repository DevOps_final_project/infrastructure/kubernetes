# I have provided two methods for deploying Kubernetes

## Deploy with helm-chart
~~~
https://gitlab.com/DevOps_final_project/infrastructure/kubernetes/-/tree/helm-chart?ref_type=heads
~~~

## Deploy with Terraform
~~~
https://gitlab.com/DevOps_final_project/infrastructure/kubernetes/-/tree/Terraform?ref_type=heads
~~~